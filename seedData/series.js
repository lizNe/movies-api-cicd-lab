const series = [
    {
      "adult": false,
      "backdrop_path": "/jWXrQstj7p3Wl5MfYWY6IHqRpDb.jpg",
      "first_air_date": "1952-12-26",
      "genre_ids": [
        10763
      ],
      "id": 94722,
      "name": "Tagesschau",
      "origin_country": [
        "DE"
      ],
      "original_language": "de",
      "original_name": "Tagesschau",
      "overview": "German daily news program, the oldest still existing program on German television.",
      "popularity": 3404.113,
      "poster_path": "/7dFZJ2ZJJdcmkp05B9NWlqTJ5tq.jpg",
      "vote_average": 7,
      "vote_count": 184
    },
    {
      "backdrop_path": "/218ZehBKlH8efPRRccmB7bu0oLQ.jpg",
      "first_air_date": "2023-09-25",
      "genre_ids": [
        35,
        9648,
        10766,
        18
      ],
      "id": 219109,
      "name": "Elas por Elas",
      "origin_country": [
        "BR"
      ],
      "original_language": "pt",
      "original_name": "Elas por Elas",
      "overview": "Seven friends who met in their youth at an English course meet again 25 years later; Lara, Taís, Helena, Adriana, Renée, Natália and Carol, each of them has a different personality and origin, but they share a deep affection.",
      "popularity": 3155.239,
      "poster_path": "/m0cvvnhnRXdQhLARx7qt9lz7hTE.jpg",
      "vote_average": 5.7,
      "vote_count": 24
    },
    {
      "backdrop_path": "/l7LRGYJY3NzIGBlpvHpMsNXHbm5.jpg",
      "first_air_date": "2023-01-09",
      "genre_ids": [
        10751,
        35
      ],
      "id": 218145,
      "name": "Mom for rent",
      "origin_country": [
        "SK"
      ],
      "original_language": "sk",
      "original_name": "Mama na prenájom",
      "overview": "Abandoned by his wife, Martin is lying to his daughter not to be upset. But as Hanka grows, these lies become unbearable. Martin meets Nada unexpectedly, asked her to be a rent-a-mother and all lives are completely changed.",
      "popularity": 2329.51,
      "poster_path": "/fH7PP2Rkdlo414IHvZABBHhtoqd.jpg",
      "vote_average": 5.2,
      "vote_count": 23
    },
    {
      "backdrop_path": "/oOce9hLMVFubjAJliau4kiSNPnW.jpg",
      "first_air_date": "1990-09-13",
      "genre_ids": [
        80,
        18
      ],
      "id": 549,
      "name": "Law & Order",
      "origin_country": [
        "US"
      ],
      "original_language": "en",
      "original_name": "Law & Order",
      "overview": "In cases ripped from the headlines, police investigate serious and often deadly crimes, weighing the evidence and questioning the suspects until someone is taken into custody. The district attorney's office then builds a case to convict the perpetrator by proving the person guilty beyond a reasonable doubt. Working together, these expert teams navigate all sides of the complex criminal justice system to make New York a safer place.",
      "popularity": 2287.191,
      "poster_path": "/77OPlbsvX3pzoFbyfpcE3GXMCod.jpg",
      "vote_average": 7.5,
      "vote_count": 475
    },
    {
      "backdrop_path": "/hwUQsL1cWt0bmbTxzCompqqvTH9.jpg",
      "first_air_date": "2004-01-22",
      "genre_ids": [
        10764
      ],
      "id": 82250,
      "name": "Gran hermano VIP",
      "origin_country": [
        "ES"
      ],
      "original_language": "es",
      "original_name": "Gran hermano VIP",
      "overview": "",
      "popularity": 2169.959,
      "poster_path": "/9269PATr0bmEXKjkpR88mzGmNYI.jpg",
      "vote_average": 4,
      "vote_count": 14
    },
    {
      "backdrop_path": "/9TXcHOeCsM8W3ZKKIKjdYUsRSeq.jpg",
      "first_air_date": "2017-07-17",
      "genre_ids": [
        80,
        18
      ],
      "id": 72879,
      "name": "Tomorrow is Ours",
      "origin_country": [
        "FR"
      ],
      "original_language": "fr",
      "original_name": "Demain nous appartient",
      "overview": "The story revolves around the people of Sète, France. Their lives are punctuated by family rivalries, romance and scenes from daily life, but also by plots involving police investigations, secrets and betrayals.",
      "popularity": 2072.563,
      "poster_path": "/3uU5uJzOX7xe7mn7YKpBM9oiEZO.jpg",
      "vote_average": 6.1,
      "vote_count": 47
    },
    {
      "backdrop_path": "/aWPhMZ0P2DyfWB7k5NXhGHSZHGC.jpg",
      "first_air_date": "2023-05-08",
      "genre_ids": [
        18,
        80,
        10766
      ],
      "id": 209265,
      "name": "Land of Desire",
      "origin_country": [
        "BR"
      ],
      "original_language": "pt",
      "original_name": "Terra e Paixão",
      "overview": "When her husband is killed in a land grabbing attempt, Aline takes charge of cultivating his land and protecting his family. Facing the powerful Antonio La Selva, responsible for the death of her husband and the largest landowner in the region, Aline is determined to keep possession of her land and invest in its production. However, she didn't expect that she would fall in love with Daniel, son of her rival, who is at odds with his rebellious half-brother, Caio, who, in turn, also falls in love with the girl. In the interior of Brazil, Aline will have to fight two battles: the dispute for her lands and for her heart.",
      "popularity": 2052.638,
      "poster_path": "/33HrrOZQKRp7W3dNXPmKB0udA2m.jpg",
      "vote_average": 6.5,
      "vote_count": 132
    },
    {
      "backdrop_path": "/yii7eIlaw1MRMfa7FTA6mW8hBUQ.jpg",
      "first_air_date": "2023-08-14",
      "genre_ids": [
        35,
        10766
      ],
      "id": 213026,
      "name": "Fuzue",
      "origin_country": [
        "BR"
      ],
      "original_language": "pt",
      "original_name": "Fuzuê",
      "overview": "The department store Fuzue, specialized in popular items, is a success led by Nero Braga e Silva. Little does he know that the store's grounds have hidden relics for over 300 years.",
      "popularity": 1841.077,
      "poster_path": "/zNNFg8z3z7uT817n6M0kHRcYwq1.jpg",
      "vote_average": 5.6,
      "vote_count": 22
    },
    {
      "backdrop_path": "/69Jblm3seQgiPuPQMrJqg9Nxhaz.jpg",
      "first_air_date": "2011-01-10",
      "genre_ids": [
        10763,
        10767
      ],
      "id": 101463,
      "name": "Al rojo vivo",
      "origin_country": [
        "ES"
      ],
      "original_language": "es",
      "original_name": "Al rojo vivo",
      "overview": "\"Al rojo vivo\" is a program on laSexta focused on the analysis and debate of national and international political current affairs. The format is hosted and directed by Antonio García Ferreras and produced by the News Services of laSexta.",
      "popularity": 1827.365,
      "poster_path": "/ag6PmoBxkF2s1uY3An618NCEt3g.jpg",
      "vote_average": 4.2,
      "vote_count": 25
    },
    {
      "backdrop_path": "/2GflJk6Hm1L3FFVWIvnuOskDFGv.jpg",
      "first_air_date": "2023-08-28",
      "genre_ids": [
        18,
        9648
      ],
      "id": 226773,
      "name": "Senior High",
      "origin_country": [
        "PH"
      ],
      "original_language": "tl",
      "original_name": "Senior High",
      "overview": "A student’s death causes a scandal at the prestigious Northford High. Investigations conclude it was a suicide. The victim’s twin sister thinks otherwise. As she searches for truth, she will unravel secrets that are far more shocking and dangerous.",
      "popularity": 1783.953,
      "poster_path": "/k285iD6gZIoLsVSczSjc4WIXkdc.jpg",
      "vote_average": 6.6,
      "vote_count": 5
    },
    {
      "backdrop_path": "/d9Oa0KJkSO0QWAxCnRNuWUOBnYw.jpg",
      "first_air_date": "2023-11-03",
      "genre_ids": [
        10764
      ],
      "id": 238766,
      "name": "Temptation Island India",
      "origin_country": [
        "IN"
      ],
      "original_language": "hi",
      "original_name": "Temptation Island India",
      "overview": "Relationships ka hoga ab ultimate test! Will their love survive the temptations? Watch the biggest and hottest dating reality show in India only on JioCinema!",
      "popularity": 1716.819,
      "poster_path": "/lpcg76guqxN9YuKu6795TQaYVOR.jpg",
      "vote_average": 5,
      "vote_count": 4
    },
    {
      "backdrop_path": null,
      "first_air_date": "2023-10-15",
      "genre_ids": [],
      "id": 237200,
      "name": "Stardance XII ...kolem dokola",
      "origin_country": [
        "CZ"
      ],
      "original_language": "cs",
      "original_name": "Stardance XII ...kolem dokola",
      "overview": "",
      "popularity": 1711.463,
      "poster_path": "/ot9MBwaVWb8vJU3mrUPXSuIcxUP.jpg",
      "vote_average": 5.2,
      "vote_count": 3
    },
    {
      "backdrop_path": "/rj3jBAZwPiOgkwAy1205MAgLahj.jpg",
      "first_air_date": "2018-08-27",
      "genre_ids": [
        10766
      ],
      "id": 81329,
      "name": "Chronicles of the Sun",
      "origin_country": [
        "FR"
      ],
      "original_language": "fr",
      "original_name": "Un si grand soleil",
      "overview": "",
      "popularity": 1661.902,
      "poster_path": "/t6jVlbPMtZOJoAOfeoR4yQmnjXM.jpg",
      "vote_average": 7.4,
      "vote_count": 52
    },
    {
      "backdrop_path": "/xYhbLfLbamisMHCTrb7yqdYuYCw.jpg",
      "first_air_date": "2001-10-22",
      "genre_ids": [
        10764
      ],
      "id": 12513,
      "name": "Operación triunfo",
      "origin_country": [
        "ES"
      ],
      "original_language": "es",
      "original_name": "Operación triunfo",
      "overview": "Reality-show talent contest aimed to find the country's next solo singing sensation, putting a selection of hopefuls through their paces by getting them to sing a variety of cover versions of popular songs, with tutoring from various professionals.",
      "popularity": 1660.422,
      "poster_path": "/2OHQg65fxUlAGpAJxDs3dafbJsB.jpg",
      "vote_average": 5.3,
      "vote_count": 11
    },
    {
      "backdrop_path": "/zKBkTNmsNdUjkW4mBBDEh1WwBPP.jpg",
      "first_air_date": "2023-08-07",
      "genre_ids": [
        18,
        9648
      ],
      "id": 229947,
      "name": "The Elegant Empire",
      "origin_country": [
        "KR"
      ],
      "original_language": "ko",
      "original_name": "우아한 제국",
      "overview": "A man and a woman take revenge desperately yet elegantly after being trampled on by enormous power.",
      "popularity": 1654.436,
      "poster_path": "/w4HHWY2sZrwPvvA9qy4dpYPSVPp.jpg",
      "vote_average": 9.3,
      "vote_count": 4
    },
    {
      "backdrop_path": "/4W2sH4CXzJ98ScuLGRij1KakzSv.jpg",
      "first_air_date": "2023-09-18",
      "genre_ids": [
        10751,
        35
      ],
      "id": 230525,
      "name": "Unpredictable Family",
      "origin_country": [
        "KR"
      ],
      "original_language": "ko",
      "original_name": "우당탕탕 패밀리",
      "overview": "A romantic family drama about a divorced couple who broke up 30 years ago out of hate, reuniting as in-laws through their children and overcoming long overdue conflicts and enmity.",
      "popularity": 1627.55,
      "poster_path": "/goMzJ6rxTndGki2pKpyAKuKNXHY.jpg",
      "vote_average": 7.3,
      "vote_count": 3
    },
    {
      "backdrop_path": "/eWF3oRyL4QWaidN9F4uvM7cBJUV.jpg",
      "first_air_date": "2005-10-13",
      "genre_ids": [
        10766
      ],
      "id": 206559,
      "name": "Binnelanders",
      "origin_country": [
        "ZA"
      ],
      "original_language": "af",
      "original_name": "Binnelanders",
      "overview": "A South African Afrikaans soap opera. It is set in and around the fictional private hospital, Binneland Kliniek, in Pretoria, and the storyline follows the trials, trauma and tribulations of the staff and patients of the hospital.",
      "popularity": 1614.399,
      "poster_path": "/v9nGSRx5lFz6KEgfmgHJMSgaARC.jpg",
      "vote_average": 5.4,
      "vote_count": 19
    },
    {
      "backdrop_path": "/46PJ9taXITt1aXPZTMJXpBw5MlU.jpg",
      "first_air_date": "2023-08-21",
      "genre_ids": [],
      "id": 232937,
      "name": "Minas de Pasión",
      "origin_country": [
        "MX"
      ],
      "original_language": "es",
      "original_name": "Minas de Pasión",
      "overview": "Emilia a single mother who works in a mine fall in love with Leonardo the son of Roberta Castro the most powerful of the people. Roberta will seek revenge on Emilia by making her life miserable.",
      "popularity": 1593.41,
      "poster_path": "/lLJBisfhikqTD0Cj9a2ZJw5kyOe.jpg",
      "vote_average": 7.5,
      "vote_count": 29
    },
    {
      "backdrop_path": null,
      "first_air_date": "2005-09-05",
      "genre_ids": [
        18,
        35
      ],
      "id": 36361,
      "name": "Ulice",
      "origin_country": [
        "CZ"
      ],
      "original_language": "cs",
      "original_name": "Ulice",
      "overview": "Ulice is a Czech soap opera produced and broadcast by Nova. In the Czech language Ulice means street.\n\nThe show describes the lives of the Farský, Jordán, Boháč, Nikl, and Liška families and many other people that live in Prague. Their daily battle against real problems of living in a modern world like divorce, love, betrayal and illness or disease. Ulice often shows crime.",
      "popularity": 1485.368,
      "poster_path": "/3ayWL13P1HeRnyVL9lU9flOdZjq.jpg",
      "vote_average": 2.5,
      "vote_count": 16
    },
    {
      "backdrop_path": "/qZ7CdO7rkH0KyCxvZBoP4Mm2cQL.jpg",
      "first_air_date": "2021-08-30",
      "genre_ids": [
        10767,
        99
      ],
      "id": 132544,
      "name": "Great Minds",
      "origin_country": [
        "KR"
      ],
      "original_language": "ko",
      "original_name": "위대한 수업, 그레이트 마인즈",
      "overview": "",
      "popularity": 1465.177,
      "poster_path": "/lj46PSaBziqCqbGXlmnXN325biA.jpg",
      "vote_average": 4.3,
      "vote_count": 8
    }


];

export default series;
