
const actors = [

    {
        
            "adult": false,
            "gender": 1,
            "id": 974169,
            "known_for_department": "Acting",
            "name": "Jenna Ortega",
            "original_name": "Jenna Ortega",
            "popularity": 163.813,
            "profile_path": "/q1NRzyZQlYkxLY07GO9NVPkQnu8.jpg",
            "known_for": [
              {
                "adult": false,
                "backdrop_path": "/iHSwvRVsRyxpX7FE7GbviaDvgGZ.jpg",
                "id": 119051,
                "name": "Wednesday",
                "original_language": "en",
                "original_name": "Wednesday",
                "overview": "Wednesday Addams is sent to Nevermore Academy, a bizarre boarding school where she attempts to master her psychic powers, stop a monstrous killing spree of the town citizens, and solve the supernatural mystery that affected her family 25 years ago — all while navigating her new relationships.",
                "poster_path": "/9PFonBhy4cQy7Jz20NpMygczOkv.jpg",
                "media_type": "tv",
                "genre_ids": [
                  10765,
                  9648,
                  35
                ],
                "popularity": 449.585,
                "first_air_date": "2022-11-23",
                "vote_average": 8.525,
                "vote_count": 7740,
                "origin_country": [
                  "US"
                ]
              },
              {
                "adult": false,
                "backdrop_path": "/ifUfE79O1raUwbaQRIB7XnFz5ZC.jpg",
                "id": 646385,
                "title": "Scream",
                "original_language": "en",
                "original_title": "Scream",
                "overview": "Twenty-five years after a streak of brutal murders shocked the quiet town of Woodsboro, a new killer has donned the Ghostface mask and begins targeting a group of teenagers to resurrect secrets from the town’s deadly past.",
                "poster_path": "/971Kqs1q4nuSc9arn1QAuKYbfxy.jpg",
                "media_type": "movie",
                "genre_ids": [
                  27,
                  9648,
                  53
                ],
                "popularity": 79.735,
                "release_date": "2022-01-12",
                "video": false,
                "vote_average": 6.698,
                "vote_count": 2887
              },
              {
                "adult": false,
                "backdrop_path": "/70Rm9ItxKuEKN8iu6rNjfwAYUCJ.jpg",
                "id": 760104,
                "title": "X",
                "original_language": "en",
                "original_title": "X",
                "overview": "In 1979, a group of young filmmakers set out to make an adult film in rural Texas, but when their reclusive, elderly hosts catch them in the act, the cast find themselves fighting for their lives.",
                "poster_path": "/woTQx9Q4b8aO13jR9dsj8C9JESy.jpg",
                "media_type": "movie",
                "genre_ids": [
                  27,
                  53,
                  9648
                ],
                "popularity": 121.589,
                "release_date": "2022-03-17",
                "video": false,
                "vote_average": 6.752,
                "vote_count": 2431
              }
            ]
          },
          {
            "adult": false,
            "gender": 2,
            "id": 27972,
            "known_for_department": "Acting",
            "name": "Josh Hutcherson",
            "original_name": "Josh Hutcherson",
            "popularity": 155.351,
            "profile_path": "/eg0P4mpVQjq2zEtwhQLrJz10Ld5.jpg",
            "known_for": [
              {
                "adult": false,
                "backdrop_path": "/yDbyVT8tTOgXUrUXNkHEUqbxb1K.jpg",
                "id": 70160,
                "title": "The Hunger Games",
                "original_language": "en",
                "original_title": "The Hunger Games",
                "overview": "Every year in the ruins of what was once North America, the nation of Panem forces each of its twelve districts to send a teenage boy and girl to compete in the Hunger Games.  Part twisted entertainment, part government intimidation tactic, the Hunger Games are a nationally televised event in which “Tributes” must fight with one another until one survivor remains.  Pitted against highly-trained Tributes who have prepared for these Games their entire lives, Katniss is forced to rely upon her sharp instincts as well as the mentorship of drunken former victor Haymitch Abernathy.  If she’s ever to return home to District 12, Katniss must make impossible choices in the arena that weigh survival against humanity and life against love. The world will be watching.",
                "poster_path": "/yXCbOiVDCxO71zI7cuwBRXdftq8.jpg",
                "media_type": "movie",
                "genre_ids": [
                  878,
                  12,
                  14
                ],
                "popularity": 89.128,
                "release_date": "2012-03-12",
                "video": false,
                "vote_average": 7.199,
                "vote_count": 20904
              },
              {
                "adult": false,
                "backdrop_path": "/dIi0De3LzEVSQHEUlh0Q2zUpmeW.jpg",
                "id": 101299,
                "title": "The Hunger Games: Catching Fire",
                "original_language": "en",
                "original_title": "The Hunger Games: Catching Fire",
                "overview": "Katniss Everdeen has returned home safe after winning the 74th Annual Hunger Games along with fellow tribute Peeta Mellark. Winning means that they must turn around and leave their family and close friends, embarking on a \"Victor's Tour\" of the districts. Along the way Katniss senses that a rebellion is simmering, but the Capitol is still very much in control as President Snow prepares the 75th Annual Hunger Games (The Quarter Quell) - a competition that could change Panem forever.",
                "poster_path": "/vrQHDXjVmbYzadOXQ0UaObunoy2.jpg",
                "media_type": "movie",
                "genre_ids": [
                  12,
                  28,
                  878
                ],
                "popularity": 143.186,
                "release_date": "2013-11-15",
                "video": false,
                "vote_average": 7.424,
                "vote_count": 16412
              },
              {
                "adult": false,
                "backdrop_path": "/tFlSDoWQsAZ2qjICKzfP5Yw6zM5.jpg",
                "id": 131631,
                "title": "The Hunger Games: Mockingjay - Part 1",
                "original_language": "en",
                "original_title": "The Hunger Games: Mockingjay - Part 1",
                "overview": "Katniss Everdeen reluctantly becomes the symbol of a mass rebellion against the autocratic Capitol.",
                "poster_path": "/4FAA18ZIja70d1Tu5hr5cj2q1sB.jpg",
                "media_type": "movie",
                "genre_ids": [
                  878,
                  12,
                  53
                ],
                "popularity": 236.931,
                "release_date": "2014-11-19",
                "video": false,
                "vote_average": 6.802,
                "vote_count": 14942
              }
            ]
          },
          {
            "adult": false,
            "gender": 1,
            "id": 2359226,
            "known_for_department": "Acting",
            "name": "Aya Asahina",
            "original_name": "Aya Asahina",
            "popularity": 146.579,
            "profile_path": "/zF3la0KvayUV3uACYPiBgCRIQcI.jpg",
            "known_for": [
              {
                "adult": false,
                "backdrop_path": "/bKxiLRPVWe2nZXCzt6JPr5HNWYm.jpg",
                "id": 110316,
                "name": "Alice in Borderland",
                "original_language": "ja",
                "original_name": "今際の国のアリス",
                "overview": "With his two friends, a video-game-obsessed young man finds himself in a strange version of Tokyo where they must compete in dangerous games to win.",
                "poster_path": "/20mOwAAPwZ1vLQkw0fvuQHiG7bO.jpg",
                "media_type": "tv",
                "genre_ids": [
                  18,
                  9648,
                  10759
                ],
                "popularity": 413.604,
                "first_air_date": "2020-12-10",
                "vote_average": 8.2,
                "vote_count": 1713,
                "origin_country": [
                  "JP"
                ]
              },
              {
                "adult": false,
                "backdrop_path": null,
                "id": 677602,
                "title": "Grand Blue",
                "original_language": "ja",
                "original_title": "ぐらんぶる",
                "overview": "Iori’s only dream is to go to college on a remote island—but when he gets roped into the school’s debaucherous, alcohol-indulgent diving club his hope for a sparkling campus life is thrown into chaos.",
                "poster_path": "/iZGnDnstcdorT0zJRpWzSQlMNz6.jpg",
                "media_type": "movie",
                "genre_ids": [
                  35
                ],
                "popularity": 14.168,
                "release_date": "2020-08-07",
                "video": false,
                "vote_average": 6,
                "vote_count": 12
              },
              {
                "adult": false,
                "backdrop_path": "/iNpbnWI2vQWGdPC9Pbt1RnV71R5.jpg",
                "id": 91414,
                "name": "Runway 24",
                "original_language": "ja",
                "original_name": "ランウェイ 24",
                "overview": "Inoue Momoko admires her late father, who was a pilot. She begins work as a co-pilot at a low-cost airline. Under Captain Shinkai Kohei’s instructions, who knew her father, Inoue Momoko works hard to become a captain and her boyfriend Umino Daisuke supports her dream too.\n\nOne day, Inoue Momoko has a problem with a passenger complaining about the limit for carry-on baggage. At that time, Katsuki Tetsuya looks at the situation.",
                "poster_path": "/2j2p3MF2GpSMHAFaYTgIRQeVOHa.jpg",
                "media_type": "tv",
                "genre_ids": [
                  10751,
                  18
                ],
                "popularity": 4.977,
                "first_air_date": "2019-07-06",
                "vote_average": 6.7,
                "vote_count": 3,
                "origin_country": [
                  "JP"
                ]
              }
            ]
          },
          {
            "adult": false,
            "gender": 2,
            "id": 1878952,
            "known_for_department": "Acting",
            "name": "Song Kang",
            "original_name": "Song Kang",
            "popularity": 142.593,
            "profile_path": "/dMUXkMfkTEw3qrJisk0mQCQxmBI.jpg",
            "known_for": [
              {
                "adult": false,
                "backdrop_path": "/mceCXNTny6a5F3rQgShLoyARw4l.jpg",
                "id": 96648,
                "name": "Sweet Home",
                "original_language": "ko",
                "original_name": "스위트홈",
                "overview": "As humans turn into savage monsters and the world plunges into terror, a handful of survivors fight for their lives — and to hold on to their humanity.",
                "poster_path": "/u8sLAJUvY9yzWqtVfKRQz5yin3D.jpg",
                "media_type": "tv",
                "genre_ids": [
                  18,
                  10765,
                  80
                ],
                "popularity": 864.966,
                "first_air_date": "2020-12-18",
                "vote_average": 8.398,
                "vote_count": 1115,
                "origin_country": [
                  "KR"
                ]
              },
              {
                "adult": false,
                "backdrop_path": "/mXouvrZbn8YpZMURGvw30QK8qfo.jpg",
                "id": 89641,
                "name": "Love Alarm",
                "original_language": "ko",
                "original_name": "좋아하면 울리는",
                "overview": "Love Alarm is an app that tells you if someone within a 10-meter radius has a crush on you. It quickly becomes a social phenomenon. While everyone talks about it and uses it to test their love and popularity, Jojo is one of the few people who have yet to download the app. However, she soon faces a love triangle situation between Sun-oh whom she starts to have feelings for, and Hye-young, who has had a huge crush on her.",
                "poster_path": "/s87JyAWtRLLlmsYWXTED1l8henB.jpg",
                "media_type": "tv",
                "genre_ids": [
                  18,
                  10765
                ],
                "popularity": 64.367,
                "first_air_date": "2019-08-22",
                "vote_average": 8.406,
                "vote_count": 1858,
                "origin_country": [
                  "KR"
                ]
              },
              {
                "adult": false,
                "backdrop_path": "/iSAH9nH10hke3IW9woUSnTmSHm4.jpg",
                "id": 119464,
                "name": "Navillera",
                "original_language": "ko",
                "original_name": "나빌레라",
                "overview": "A 70-year-old with a dream and a 23-year-old with a gift lift each other out of harsh realities and rise to the challenge of becoming ballerinos.",
                "poster_path": "/pBd6pEhszsWk95TrUqo4bdXhH8T.jpg",
                "media_type": "tv",
                "genre_ids": [
                  18
                ],
                "popularity": 29.149,
                "first_air_date": "2021-03-22",
                "vote_average": 8.6,
                "vote_count": 168,
                "origin_country": [
                  "KR"
                ]
              }
            ]
          },
          {
            "adult": false,
            "gender": 1,
            "id": 2604515,
            "known_for_department": "Acting",
            "name": "Emma Myers",
            "original_name": "Emma Myers",
            "popularity": 139.254,
            "profile_path": "/e5eTDKstLsUKKfmwgiJsDakcvly.jpg",
            "known_for": [
              {
                "adult": false,
                "backdrop_path": "/iHSwvRVsRyxpX7FE7GbviaDvgGZ.jpg",
                "id": 119051,
                "name": "Wednesday",
                "original_language": "en",
                "original_name": "Wednesday",
                "overview": "Wednesday Addams is sent to Nevermore Academy, a bizarre boarding school where she attempts to master her psychic powers, stop a monstrous killing spree of the town citizens, and solve the supernatural mystery that affected her family 25 years ago — all while navigating her new relationships.",
                "poster_path": "/9PFonBhy4cQy7Jz20NpMygczOkv.jpg",
                "media_type": "tv",
                "genre_ids": [
                  10765,
                  9648,
                  35
                ],
                "popularity": 449.585,
                "first_air_date": "2022-11-23",
                "vote_average": 8.525,
                "vote_count": 7740,
                "origin_country": [
                  "US"
                ]
              },
              {
                "adult": false,
                "backdrop_path": "/fvUmnxS9xaUUl0eWygvcA1hPdTV.jpg",
                "id": 801335,
                "title": "Girl in the Basement",
                "original_language": "en",
                "original_title": "Girl in the Basement",
                "overview": "Sara is a teen girl who is looking forward to her 18th birthday to move away from her controlling father Don. But before she could even blow out the candles, Don imprisons her in the basement of their home.",
                "poster_path": "/qmddUxRwbsxHa7oEXm4PWh1KZe8.jpg",
                "media_type": "movie",
                "genre_ids": [
                  80,
                  53,
                  10770
                ],
                "popularity": 82.708,
                "release_date": "2021-02-27",
                "video": false,
                "vote_average": 7.722,
                "vote_count": 592
              },
              {
                "adult": false,
                "backdrop_path": "/8WBh82iOYRykS3C05RipWTX70xh.jpg",
                "id": 798021,
                "title": "Family Switch",
                "original_language": "en",
                "original_title": "Family Switch",
                "overview": "When the Walker family members switch bodies with each other during a rare planetary alignment, their hilarious journey to find their way back to normal will bring them closer together than they ever thought possible.",
                "poster_path": "/fnRUCA0fjEb3kuIaTGogL7425IC.jpg",
                "media_type": "movie",
                "genre_ids": [
                  35,
                  14,
                  10751,
                  80
                ],
                "popularity": 434.155,
                "release_date": "2023-11-30",
                "video": false,
                "vote_average": 6.307,
                "vote_count": 148
              }
            ]
          },
          {
            "adult": false,
            "gender": 2,
            "id": 73968,
            "known_for_department": "Acting",
            "name": "Henry Cavill",
            "original_name": "Henry Cavill",
            "popularity": 135.433,
            "profile_path": "/iWdKjMry5Pt7vmxU7bmOQsIUyHa.jpg",
            "known_for": [
              {
                "adult": false,
                "backdrop_path": "/69EFgWWPFWbRNHmQgYdSnyJ94Ge.jpg",
                "id": 49521,
                "title": "Man of Steel",
                "original_language": "en",
                "original_title": "Man of Steel",
                "overview": "A young boy learns that he has extraordinary powers and is not of this earth. As a young man, he journeys to discover where he came from and what he was sent here to do. But the hero in him must emerge if he is to save the world from annihilation and become the symbol of hope for all mankind.",
                "poster_path": "/dksTL9NXc3GqPBRHYHcy1aIwjS.jpg",
                "media_type": "movie",
                "genre_ids": [
                  28,
                  12,
                  878
                ],
                "popularity": 102.904,
                "release_date": "2013-06-12",
                "video": false,
                "vote_average": 6.616,
                "vote_count": 14440
              },
              {
                "adult": false,
                "backdrop_path": "/5fX1oSGuYdKgwWmUTAN5MNSQGzr.jpg",
                "id": 209112,
                "title": "Batman v Superman: Dawn of Justice",
                "original_language": "en",
                "original_title": "Batman v Superman: Dawn of Justice",
                "overview": "Fearing the actions of a god-like Super Hero left unchecked, Gotham City’s own formidable, forceful vigilante takes on Metropolis’s most revered, modern-day savior, while the world wrestles with what sort of hero it really needs. And with Batman and Superman at war with one another, a new threat quickly arises, putting mankind in greater danger than it’s ever known before.",
                "poster_path": "/5UsK3grJvtQrtzEgqNlDljJW96w.jpg",
                "media_type": "movie",
                "genre_ids": [
                  28,
                  12,
                  14
                ],
                "popularity": 61.841,
                "release_date": "2016-03-23",
                "video": false,
                "vote_average": 5.957,
                "vote_count": 17218
              },
              {
                "adult": false,
                "backdrop_path": "/2nyaeISu2xIxIgZYNpX4UayY8PN.jpg",
                "id": 141052,
                "title": "Justice League",
                "original_language": "en",
                "original_title": "Justice League",
                "overview": "Fuelled by his restored faith in humanity and inspired by Superman's selfless act, Bruce Wayne and Diana Prince assemble a team of metahumans consisting of Barry Allen, Arthur Curry and Victor Stone to face the catastrophic threat of Steppenwolf and the Parademons who are on the hunt for three Mother Boxes on Earth.",
                "poster_path": "/eifGNCSDuxJeS1loAXil5bIGgvC.jpg",
                "media_type": "movie",
                "genre_ids": [
                  28,
                  12,
                  878
                ],
                "popularity": 81.885,
                "release_date": "2017-11-15",
                "video": false,
                "vote_average": 6.095,
                "vote_count": 12357
              }
            ]
          },
          {
            "adult": false,
            "gender": 2,
            "id": 976,
            "known_for_department": "Acting",
            "name": "Jason Statham",
            "original_name": "Jason Statham",
            "popularity": 134.693,
            "profile_path": "/whNwkEQYWLFJA8ij0WyOOAD5xhQ.jpg",
            "known_for": [
              {
                "adult": false,
                "backdrop_path": "/ysKahAEPP8h6MInuLjr0xuZOTjh.jpg",
                "id": 107,
                "title": "Snatch",
                "original_language": "en",
                "original_title": "Snatch",
                "overview": "Unscrupulous boxing promoters, violent bookmakers, a Russian gangster, incompetent amateur robbers and supposedly Jewish jewelers fight to track down a priceless stolen diamond.",
                "poster_path": "/56mOJth6DJ6JhgoE2jtpilVqJO.jpg",
                "media_type": "movie",
                "genre_ids": [
                  80,
                  35
                ],
                "popularity": 39.83,
                "release_date": "2000-09-01",
                "video": false,
                "vote_average": 7.805,
                "vote_count": 8428
              },
              {
                "adult": false,
                "backdrop_path": "/iP1cjN3ZGs1yJXROKaUj9hi1yF2.jpg",
                "id": 345940,
                "title": "The Meg",
                "original_language": "en",
                "original_title": "The Meg",
                "overview": "A deep sea submersible pilot revisits his past fears in the Mariana Trench, and accidentally unleashes the seventy foot ancestor of the Great White Shark believed to be extinct.",
                "poster_path": "/eyWICPcxOuTcDDDbTMOZawoOn8d.jpg",
                "media_type": "movie",
                "genre_ids": [
                  28,
                  878,
                  27
                ],
                "popularity": 81.319,
                "release_date": "2018-08-09",
                "video": false,
                "vote_average": 6.255,
                "vote_count": 7156
              },
              {
                "adult": false,
                "backdrop_path": "/kWt1OcPgwO1ssu57wgTKmq38JYw.jpg",
                "id": 4108,
                "title": "The Transporter",
                "original_language": "en",
                "original_title": "The Transporter",
                "overview": "Former Special Forces officer, Frank Martin will deliver anything to anyone for the right price, and his no-questions-asked policy puts him in high demand. But when he realizes his latest cargo is alive, it sets in motion a dangerous chain of events. The bound and gagged Lai is being smuggled to France by a shady American businessman, and Frank works to save her as his own illegal activities are uncovered by a French detective.",
                "poster_path": "/v3QIFUWgtVN4wejVuDZowuyJ20W.jpg",
                "media_type": "movie",
                "genre_ids": [
                  28,
                  80,
                  53
                ],
                "popularity": 33.44,
                "release_date": "2002-10-02",
                "video": false,
                "vote_average": 6.698,
                "vote_count": 4976
              }
            ]
          },
          {
            "adult": false,
            "gender": 1,
            "id": 4068148,
            "known_for_department": "Acting",
            "name": "Madeleine Yuna Voyles",
            "original_name": "Madeleine Yuna Voyles",
            "popularity": 133.014,
            "profile_path": "/mLKvNuCJSgyK0WdLd4ogO81sOO1.jpg",
            "known_for": [
              {
                "adult": false,
                "backdrop_path": "/kjQBrc00fB2RjHZB3PGR4w9ibpz.jpg",
                "id": 670292,
                "title": "The Creator",
                "original_language": "en",
                "original_title": "The Creator",
                "overview": "Amid a future war between the human race and the forces of artificial intelligence, a hardened ex-special forces agent grieving the disappearance of his wife, is recruited to hunt down and kill the Creator, the elusive architect of advanced AI who has developed a mysterious weapon with the power to end the war—and mankind itself.",
                "poster_path": "/vBZ0qvaRxqEhZwl6LWmruJqWE8Z.jpg",
                "media_type": "movie",
                "genre_ids": [
                  878,
                  28,
                  53
                ],
                "popularity": 779.137,
                "release_date": "2023-09-27",
                "video": false,
                "vote_average": 7.137,
                "vote_count": 1295
              }
            ]
          },
          {
            "adult": false,
            "gender": 1,
            "id": 54693,
            "known_for_department": "Acting",
            "name": "Emma Stone",
            "original_name": "Emma Stone",
            "popularity": 130.962,
            "profile_path": "/3UaYw9KF4fEXRMRWhf25aGJpAW2.jpg",
            "known_for": [
              {
                "adult": false,
                "backdrop_path": "/6MKr3KgOLmzOP6MSuZERO41Lpkt.jpg",
                "id": 337404,
                "title": "Cruella",
                "original_language": "en",
                "original_title": "Cruella",
                "overview": "In 1970s London amidst the punk rock revolution, a young grifter named Estella is determined to make a name for herself with her designs. She befriends a pair of young thieves who appreciate her appetite for mischief, and together they are able to build a life for themselves on the London streets. One day, Estella’s flair for fashion catches the eye of the Baroness von Hellman, a fashion legend who is devastatingly chic and terrifyingly haute. But their relationship sets in motion a course of events and revelations that will cause Estella to embrace her wicked side and become the raucous, fashionable and revenge-bent Cruella.",
                "poster_path": "/wToO8opxkGwKgSfJ1JK8tGvkG6U.jpg",
                "media_type": "movie",
                "genre_ids": [
                  35,
                  80
                ],
                "popularity": 89.576,
                "release_date": "2021-05-26",
                "video": false,
                "vote_average": 8.045,
                "vote_count": 8621
              },
              {
                "adult": false,
                "backdrop_path": "/ac0kRKTfiJ4GcoUfb0XIO5vgC8q.jpg",
                "id": 1930,
                "title": "The Amazing Spider-Man",
                "original_language": "en",
                "original_title": "The Amazing Spider-Man",
                "overview": "Peter Parker is an outcast high schooler abandoned by his parents as a boy, leaving him to be raised by his Uncle Ben and Aunt May. Like most teenagers, Peter is trying to figure out who he is and how he got to be the person he is today. As Peter discovers a mysterious briefcase that belonged to his father, he begins a quest to understand his parents' disappearance – leading him directly to Oscorp and the lab of Dr. Curt Connors, his father's former partner. As Spider-Man is set on a collision course with Connors' alter ego, The Lizard, Peter will make life-altering choices to use his powers and shape his destiny to become a hero.",
                "poster_path": "/jIfkQNARYyERqRAq1p1c8xgePp4.jpg",
                "media_type": "movie",
                "genre_ids": [
                  28,
                  12,
                  14
                ],
                "popularity": 111.545,
                "release_date": "2012-06-23",
                "video": false,
                "vote_average": 6.694,
                "vote_count": 16504
              },
              {
                "adult": false,
                "backdrop_path": "/qJeU7KM4nT2C1WpOrwPcSDGFUWE.jpg",
                "id": 313369,
                "title": "La La Land",
                "original_language": "en",
                "original_title": "La La Land",
                "overview": "Mia, an aspiring actress, serves lattes to movie stars in between auditions and Sebastian, a jazz musician, scrapes by playing cocktail party gigs in dingy bars, but as success mounts they are faced with decisions that begin to fray the fragile fabric of their love affair, and the dreams they worked so hard to maintain in each other threaten to rip them apart.",
                "poster_path": "/uDO8zWDhfWwoFdKS4fzkUJt0Rf0.jpg",
                "media_type": "movie",
                "genre_ids": [
                  35,
                  18,
                  10749,
                  10402
                ],
                "popularity": 42.045,
                "release_date": "2016-11-29",
                "video": false,
                "vote_average": 7.901,
                "vote_count": 15765
              }
            ]
          },
          {
            "adult": false,
            "gender": 2,
            "id": 1914924,
            "known_for_department": "Acting",
            "name": "Miles Wei",
            "original_name": "Miles Wei",
            "popularity": 116.8,
            "profile_path": "/fT4v4LTDXGEFGHe7ZAaRTtqBFYM.jpg",
            "known_for": [
              {
                "adult": false,
                "backdrop_path": "/q0dz7lf8y44KhBBnxyq8B5bKJxf.jpg",
                "id": 128926,
                "name": "Unforgettable Love",
                "original_language": "zh",
                "original_name": "贺先生的恋恋不忘",
                "overview": "The drama revolves around He Qiao Yan, CEO of Heshi Group, and Qin Yi Yue, a child psychologist. It tells the story of a rational and indifferent man and a soft, optimistic, considerate, and meticulous psychologist, whose relationship develops from acquaintance to love. Adapted from the web novel \"Mr He's Love is Not Forgotten\" by Qin Ye.",
                "poster_path": "/yHoH1QsXuAPOBEbs6FWAqkHSYBL.jpg",
                "media_type": "tv",
                "genre_ids": [
                  18,
                  35,
                  10751
                ],
                "popularity": 30.512,
                "first_air_date": "2021-07-10",
                "vote_average": 7.8,
                "vote_count": 29,
                "origin_country": [
                  "CN"
                ]
              },
              {
                "adult": false,
                "backdrop_path": "/wRZZCP33wVM4JhnxClnUH35jueE.jpg",
                "id": 110966,
                "name": "Perfect and Casual",
                "original_language": "zh",
                "original_name": "完美先生和差不多小姐",
                "overview": "This story revolves around the cold and icy \"male god\" professor Zhang Sinian and the calm and sunny Yun Shu. After a fractious first meeting, where Yun Shu dressed down the especially finicky Sinian, she was shocked to find out that he was her university lecturer. Through a series of coincidental encounters, the two agreed to engage in a contractual marriage. As the feelings between them grow, how will they keep their relationship from those around them?",
                "poster_path": "/vFsSSAIGcsBKExxlJas5MlJmRtG.jpg",
                "media_type": "tv",
                "genre_ids": [
                  35,
                  18
                ],
                "popularity": 15.707,
                "first_air_date": "2020-09-28",
                "vote_average": 8.346,
                "vote_count": 13,
                "origin_country": [
                  "CN"
                ]
              },
              {
                "adult": false,
                "backdrop_path": "/A5sCms0ZO0uJcgfqpCE8SNujs8W.jpg",
                "id": 98830,
                "name": "Find Yourself",
                "original_language": "zh",
                "original_name": "下一站是幸福",
                "overview": "A determined entrepreneur navigates a love triangle between a young charmer and an older executive, leading her down an unconventional path to love.",
                "poster_path": "/mMfgXLuWsnDRfQPeg6Fihdh7TZn.jpg",
                "media_type": "tv",
                "genre_ids": [
                  18
                ],
                "popularity": 29.999,
                "first_air_date": "2020-01-26",
                "vote_average": 7.578,
                "vote_count": 109,
                "origin_country": [
                  "CN"
                ]
              }
            ]
          },
          {
            "adult": false,
            "gender": 1,
            "id": 17628,
            "known_for_department": "Acting",
            "name": "Mary Elizabeth Winstead",
            "original_name": "Mary Elizabeth Winstead",
            "popularity": 113.262,
            "profile_path": "/cBMtncBcUrKMTB8CifDG23THuCE.jpg",
            "known_for": [
              {
                "adult": false,
                "backdrop_path": "/5uLqjVQlKNR4MbOeuheaRihPxAx.jpg",
                "id": 333371,
                "title": "10 Cloverfield Lane",
                "original_language": "en",
                "original_title": "10 Cloverfield Lane",
                "overview": "After getting in a car accident, a woman is held in a shelter with two men, who claim the outside world is affected by a widespread chemical attack.",
                "poster_path": "/84Dhwz93vCin6T1PX6ctSvWEuNE.jpg",
                "media_type": "movie",
                "genre_ids": [
                  53,
                  878,
                  18,
                  27
                ],
                "popularity": 50.979,
                "release_date": "2016-03-10",
                "video": false,
                "vote_average": 6.983,
                "vote_count": 7446
              },
              {
                "adult": false,
                "backdrop_path": "/7W9xm2YTq54rfLFimIniMrywShv.jpg",
                "id": 22538,
                "title": "Scott Pilgrim vs. the World",
                "original_language": "en",
                "original_title": "Scott Pilgrim vs. the World",
                "overview": "As bass guitarist for a garage-rock band, Scott Pilgrim has never had trouble getting a girlfriend; usually, the problem is getting rid of them. But when Ramona Flowers skates into his heart, he finds she has the most troublesome baggage of all: an army of ex-boyfriends who will stop at nothing to eliminate him from her list of suitors.",
                "poster_path": "/g5IoYeudx9XBEfwNL0fHvSckLBz.jpg",
                "media_type": "movie",
                "genre_ids": [
                  28,
                  35,
                  10749
                ],
                "popularity": 103.176,
                "release_date": "2010-08-12",
                "video": false,
                "vote_average": 7.5,
                "vote_count": 7291
              },
              {
                "adult": false,
                "backdrop_path": "/upPtpWzVrUD8XPpoHh2CPTRk36o.jpg",
                "id": 9286,
                "title": "Final Destination 3",
                "original_language": "en",
                "original_title": "Final Destination 3",
                "overview": "A student's premonition of a deadly rollercoaster ride saves her life and a lucky few, but not from death itself – which seeks out those who escaped their fate.",
                "poster_path": "/p7ARuNKUGPGvkBiDtIDvAzYzonX.jpg",
                "media_type": "movie",
                "genre_ids": [
                  27,
                  9648
                ],
                "popularity": 52.176,
                "release_date": "2006-02-09",
                "video": false,
                "vote_average": 6.086,
                "vote_count": 3578
              }
            ]
          },
          {
            "adult": false,
            "gender": 2,
            "id": 3896,
            "known_for_department": "Acting",
            "name": "Liam Neeson",
            "original_name": "Liam Neeson",
            "popularity": 113.032,
            "profile_path": "/bboldwqSC6tdw2iL6631c98l2Mn.jpg",
            "known_for": [
              {
                "adult": false,
                "backdrop_path": "/3f92DMBTFqr3wgXpfxzrb0qv8nG.jpg",
                "id": 424,
                "title": "Schindler's List",
                "original_language": "en",
                "original_title": "Schindler's List",
                "overview": "The true story of how businessman Oskar Schindler saved over a thousand Jewish lives from the Nazis while they worked as slaves in his factory during World War II.",
                "poster_path": "/sF1U4EUQS8YHUYjNl3pMGNIQyr0.jpg",
                "media_type": "movie",
                "genre_ids": [
                  18,
                  36,
                  10752
                ],
                "popularity": 83.524,
                "release_date": "1993-12-15",
                "video": false,
                "vote_average": 8.571,
                "vote_count": 14836
              },
              {
                "adult": false,
                "backdrop_path": "/wDe8LzwuvHYYiuwyNfxdYQq8ti4.jpg",
                "id": 1893,
                "title": "Star Wars: Episode I - The Phantom Menace",
                "original_language": "en",
                "original_title": "Star Wars: Episode I - The Phantom Menace",
                "overview": "Anakin Skywalker, a young slave strong with the Force, is discovered on Tatooine. Meanwhile, the evil Sith have returned, enacting their plot for revenge against the Jedi.",
                "poster_path": "/6wkfovpn7Eq8dYNKaG5PY3q2oq6.jpg",
                "media_type": "movie",
                "genre_ids": [
                  12,
                  28,
                  878
                ],
                "popularity": 46.581,
                "release_date": "1999-05-19",
                "video": false,
                "vote_average": 6.536,
                "vote_count": 13691
              },
              {
                "adult": false,
                "backdrop_path": "/9iQWsXwjOMGDvTDdvCnpiyR0UG3.jpg",
                "id": 8681,
                "title": "Taken",
                "original_language": "en",
                "original_title": "Taken",
                "overview": "While vacationing with a friend in Paris, an American girl is kidnapped by a gang of human traffickers intent on selling her into forced prostitution. Working against the clock, her ex-spy father must pull out all the stops to save her. But with his best years possibly behind him, the job may be more than he can handle.",
                "poster_path": "/y5Va1WXDX6nZElVirPrGxf6w99B.jpg",
                "media_type": "movie",
                "genre_ids": [
                  28,
                  53
                ],
                "popularity": 73.327,
                "release_date": "2008-02-18",
                "video": false,
                "vote_average": 7.39,
                "vote_count": 10551
              }
            ]
          },
          {
            "adult": false,
            "gender": 2,
            "id": 192,
            "known_for_department": "Acting",
            "name": "Morgan Freeman",
            "original_name": "Morgan Freeman",
            "popularity": 112.284,
            "profile_path": "/jPsLqiYGSofU4s6BjrxnefMfabb.jpg",
            "known_for": [
              {
                "adult": false,
                "backdrop_path": "/dYjZ27hDw2QFaEIfzbNGwW0IkV9.jpg",
                "id": 807,
                "title": "Se7en",
                "original_language": "en",
                "original_title": "Se7en",
                "overview": "Two homicide detectives are on a desperate hunt for a serial killer whose crimes are based on the \"seven deadly sins\" in this dark and haunting film that takes viewers from the tortured remains of one victim to the next. The seasoned Det. Sommerset researches each sin in an effort to get inside the killer's mind, while his novice partner, Mills, scoffs at his efforts to unravel the case.",
                "poster_path": "/6yoghtyTpznpBik8EngEmJskVUO.jpg",
                "media_type": "movie",
                "genre_ids": [
                  80,
                  9648,
                  53
                ],
                "popularity": 77.251,
                "release_date": "1995-09-22",
                "video": false,
                "vote_average": 8.371,
                "vote_count": 19687
              },
              {
                "adult": false,
                "backdrop_path": "/kXfqcdQKsToO0OUXHcrrNCHDBzO.jpg",
                "id": 278,
                "title": "The Shawshank Redemption",
                "original_language": "en",
                "original_title": "The Shawshank Redemption",
                "overview": "Framed in the 1940s for the double murder of his wife and her lover, upstanding banker Andy Dufresne begins a new life at the Shawshank prison, where he puts his accounting skills to work for an amoral warden. During his long stretch in prison, Dufresne comes to be admired by the other inmates -- including an older prisoner named Red -- for his integrity and unquenchable sense of hope.",
                "poster_path": "/q6y0Go1tsGEsmtFryDOJo3dEmqu.jpg",
                "media_type": "movie",
                "genre_ids": [
                  18,
                  80
                ],
                "popularity": 185.164,
                "release_date": "1994-09-23",
                "video": false,
                "vote_average": 8.706,
                "vote_count": 25043
              },
              {
                "adult": false,
                "backdrop_path": "/ozVwXlfxqNsariipatGwa5px3Pm.jpg",
                "id": 240832,
                "title": "Lucy",
                "original_language": "en",
                "original_title": "Lucy",
                "overview": "A woman, accidentally caught in a dark deal, turns the tables on her captors and transforms into a merciless warrior evolved beyond human logic.",
                "poster_path": "/dhjyfcwEoW6jJ4Q7DpZTp6E58GA.jpg",
                "media_type": "movie",
                "genre_ids": [
                  28,
                  878
                ],
                "popularity": 56.523,
                "release_date": "2014-07-25",
                "video": false,
                "vote_average": 6.443,
                "vote_count": 15297
              }
            ]
          },
          {
            "adult": false,
            "gender": 0,
            "id": 3234630,
            "known_for_department": "Acting",
            "name": "Sangeeth Shobhan",
            "original_name": "Sangeeth Shobhan",
            "popularity": 110.803,
            "profile_path": "/7Vox31bH7XmgPNJzMKGa4uGyjW8.jpg",
            "known_for": [
              {
                "adult": false,
                "backdrop_path": "/jBnnkkXRZ0pV3Tw31Z2ALO638wA.jpg",
                "id": 1187075,
                "title": "MAD",
                "original_language": "te",
                "original_title": "MAD",
                "overview": "Set in an engineering college and revolves around the antics of the students there, primarily the boys, who get a kick out of torturing the hostel warden.",
                "poster_path": "/nDpOmgBfQZwOpFBcgokQGqd74r1.jpg",
                "media_type": "movie",
                "genre_ids": [
                  35,
                  10749,
                  18
                ],
                "popularity": 5.751,
                "release_date": "2023-10-06",
                "video": false,
                "vote_average": 7,
                "vote_count": 4
              },
              {
                "adult": false,
                "backdrop_path": "/1jof3bGVg67HLmvRHfTzqb2IODO.jpg",
                "id": 138179,
                "name": "Oka Chinna Family Story",
                "original_language": "te",
                "original_name": "ఒక చిన్న Family Story",
                "overview": "Mahesh and his mother embark on an adventurous journey filled with hilarious situations as they try to make quick money to repay a huge loan.",
                "poster_path": "/u1Tq2Qqb1oUJ6WSzVJqWk03LzEl.jpg",
                "media_type": "tv",
                "genre_ids": [
                  35,
                  10751
                ],
                "popularity": 7.457,
                "first_air_date": "2021-11-19",
                "vote_average": 7.5,
                "vote_count": 2,
                "origin_country": [
                  "IN"
                ]
              },
              {
                "adult": false,
                "backdrop_path": "/d7jfcyPb5ZncLyhPFNjpuIeeZ1y.jpg",
                "id": 1119091,
                "title": "Prema Vimanam",
                "original_language": "te",
                "original_title": "ప్రేమ విమానం",
                "overview": "Two kids with a dream to board a flight cross paths with a young couple who must urgently catch the flight to start a new life.",
                "poster_path": "/9eljOANAd6HafUDdmp3xnmkpnt8.jpg",
                "media_type": "movie",
                "genre_ids": [
                  18,
                  35
                ],
                "popularity": 3.123,
                "release_date": "2023-10-13",
                "video": false,
                "vote_average": 7,
                "vote_count": 1
              }
            ]
          },
          {
            "adult": false,
            "gender": 1,
            "id": 224513,
            "known_for_department": "Acting",
            "name": "Ana de Armas",
            "original_name": "Ana de Armas",
            "popularity": 109.936,
            "profile_path": "/3vxvsmYLTf4jnr163SUlBIw51ee.jpg",
            "known_for": [
              {
                "adult": false,
                "backdrop_path": "/uKbX1ha7KWyTecvpPpRCB3iFfj3.jpg",
                "id": 335984,
                "title": "Blade Runner 2049",
                "original_language": "en",
                "original_title": "Blade Runner 2049",
                "overview": "Thirty years after the events of the first film, a new blade runner, LAPD Officer K, unearths a long-buried secret that has the potential to plunge what's left of society into chaos. K's discovery leads him on a quest to find Rick Deckard, a former LAPD blade runner who has been missing for 30 years.",
                "poster_path": "/gajva2L0rPYkEWjzgFlBXCAVBE5.jpg",
                "media_type": "movie",
                "genre_ids": [
                  878,
                  18
                ],
                "popularity": 120.995,
                "release_date": "2017-10-04",
                "video": false,
                "vote_average": 7.549,
                "vote_count": 12562
              },
              {
                "adult": false,
                "backdrop_path": "/4HWAQu28e2yaWrtupFPGFkdNU7V.jpg",
                "id": 546554,
                "title": "Knives Out",
                "original_language": "en",
                "original_title": "Knives Out",
                "overview": "When renowned crime novelist Harlan Thrombey is found dead at his estate just after his 85th birthday, the inquisitive and debonair Detective Benoit Blanc is mysteriously enlisted to investigate. From Harlan's dysfunctional family to his devoted staff, Blanc sifts through a web of red herrings and self-serving lies to uncover the truth behind Harlan's untimely death.",
                "poster_path": "/pThyQovXQrw2m0s9x82twj48Jq4.jpg",
                "media_type": "movie",
                "genre_ids": [
                  35,
                  80,
                  9648
                ],
                "popularity": 51.081,
                "release_date": "2019-11-27",
                "video": false,
                "vote_average": 7.845,
                "vote_count": 11344
              },
              {
                "adult": false,
                "backdrop_path": "/3Mnbw4gOYz5BNZB9PhZ2cyBSQum.jpg",
                "id": 308266,
                "title": "War Dogs",
                "original_language": "en",
                "original_title": "War Dogs",
                "overview": "Based on the true story of two young men, David Packouz and Efraim Diveroli, who won a $300 million contract from the Pentagon to arm America's allies in Afghanistan.",
                "poster_path": "/uH9qGH5XS1iZXCSb3tgu40dxQoh.jpg",
                "media_type": "movie",
                "genre_ids": [
                  35,
                  80,
                  18
                ],
                "popularity": 38.924,
                "release_date": "2016-08-18",
                "video": false,
                "vote_average": 6.907,
                "vote_count": 4564
              }
            ]
          },
          {
            "adult": false,
            "gender": 2,
            "id": 1613836,
            "known_for_department": "Acting",
            "name": "Rowoon",
            "original_name": "Rowoon",
            "popularity": 106.482,
            "profile_path": "/sOTYcl8SGmtxpWq0EQKJhX8dztr.jpg",
            "known_for": [
              {
                "adult": false,
                "backdrop_path": "/2lQ1PwGFZKM9EakCw1j4NShEuuj.jpg",
                "id": 136369,
                "name": "Tomorrow",
                "original_language": "ko",
                "original_name": "내일",
                "overview": "Made half-human and half-spirit by accident, a young man is employed by a company of grim reapers in the underworld to carry out special missions.",
                "poster_path": "/7pOC2Wr6aavn6OPkFJ9YqhCV5hW.jpg",
                "media_type": "tv",
                "genre_ids": [
                  18,
                  10765,
                  10759
                ],
                "popularity": 51.188,
                "first_air_date": "2022-04-01",
                "vote_average": 8.561,
                "vote_count": 155,
                "origin_country": [
                  "KR"
                ]
              },
              {
                "adult": false,
                "backdrop_path": "/4r7scgI5aUrShN0YzriAatWd15e.jpg",
                "id": 129478,
                "name": "The King's Affection",
                "original_language": "ko",
                "original_name": "연모",
                "overview": "When the crown prince is killed, his twin sister assumes the throne while trying to keep her identity and affection for her first love a royal secret.",
                "poster_path": "/mcfX0IqWibPhF5H4i2Q5gUlwwxk.jpg",
                "media_type": "tv",
                "genre_ids": [
                  18
                ],
                "popularity": 132.731,
                "first_air_date": "2021-10-11",
                "vote_average": 8.313,
                "vote_count": 238,
                "origin_country": [
                  "KR"
                ]
              },
              {
                "adult": false,
                "backdrop_path": "/jJMfTkvU2exHwCFoBHsIqwaHImh.jpg",
                "id": 215001,
                "name": "Destined with You",
                "original_language": "ko",
                "original_name": "이 연애는 불가항력",
                "overview": "A lawyer bound by a centuries-old curse becomes entangled with a civil servant who holds the key to his freedom — igniting an unexpected romance.",
                "poster_path": "/fMiLKTIFB7YCBQSYoTJ7NCrPcD6.jpg",
                "media_type": "tv",
                "genre_ids": [
                  18,
                  10765,
                  35
                ],
                "popularity": 170.456,
                "first_air_date": "2023-08-23",
                "vote_average": 7.907,
                "vote_count": 70,
                "origin_country": [
                  "KR"
                ]
              }
            ]
          },
          {
            "adult": false,
            "gender": 2,
            "id": 2963,
            "known_for_department": "Acting",
            "name": "Nicolas Cage",
            "original_name": "Nicolas Cage",
            "popularity": 104.492,
            "profile_path": "/ar33qcWbEgREn07ZpXv5Pbj8hbM.jpg",
            "known_for": [
              {
                "adult": false,
                "backdrop_path": "/bNgqt819qpHcszjCzLCG5y16ldF.jpg",
                "id": 49519,
                "title": "The Croods",
                "original_language": "en",
                "original_title": "The Croods",
                "overview": "The prehistoric Croods family live in a particularly dangerous moment in time. Patriarch Grug, his mate Ugga, teenage daughter Eep, son Thunk, and feisty Gran gather food by day and huddle together in a cave at night. When a more evolved caveman named Guy arrives on the scene, Grug is distrustful, but it soon becomes apparent that Guy is correct about the impending destruction of their world.",
                "poster_path": "/p7lJkqHlK01nr0zNacunUFI5Qxy.jpg",
                "media_type": "movie",
                "genre_ids": [
                  16,
                  12,
                  10751,
                  14,
                  35,
                  28
                ],
                "popularity": 52.731,
                "release_date": "2013-03-15",
                "video": false,
                "vote_average": 6.915,
                "vote_count": 6681
              },
              {
                "adult": false,
                "backdrop_path": "/xXWT0je8dTFFNBq6P2CeTZkPUu2.jpg",
                "id": 2059,
                "title": "National Treasure",
                "original_language": "en",
                "original_title": "National Treasure",
                "overview": "Modern treasure hunters, led by archaeologist Ben Gates, search for a chest of riches rumored to have been stashed away by George Washington, Thomas Jefferson and Benjamin Franklin during the Revolutionary War. The chest's whereabouts may lie in secret clues embedded in the Constitution and the Declaration of Independence, and Gates is in a race to find the gold before his enemies do.",
                "poster_path": "/pxL6E4GBOPUG6CdkO9cUQN5VMwI.jpg",
                "media_type": "movie",
                "genre_ids": [
                  12,
                  28,
                  53,
                  9648
                ],
                "popularity": 33.705,
                "release_date": "2004-11-19",
                "video": false,
                "vote_average": 6.617,
                "vote_count": 5919
              },
              {
                "adult": false,
                "backdrop_path": "/7zS58YPAw002RNejOF1vNB3XHbW.jpg",
                "id": 1250,
                "title": "Ghost Rider",
                "original_language": "en",
                "original_title": "Ghost Rider",
                "overview": "In order to save his dying father, young stunt cyclist Johnny Blaze sells his soul to Mephistopheles and sadly parts from the pure-hearted Roxanne Simpson, the love of his life. Years later, Johnny's path crosses again with Roxanne, now a go-getting reporter, and also with Mephistopheles, who offers to release Johnny's soul if Johnny becomes the fabled, fiery 'Ghost Rider'.",
                "poster_path": "/8LaVQiXBsnlo7MXCPK1nXTVARUZ.jpg",
                "media_type": "movie",
                "genre_ids": [
                  53,
                  28,
                  14
                ],
                "popularity": 54.47,
                "release_date": "2007-01-15",
                "video": false,
                "vote_average": 5.551,
                "vote_count": 5546
              }
            ]
          },
          {
            "adult": false,
            "gender": 2,
            "id": 18897,
            "known_for_department": "Acting",
            "name": "Jackie Chan",
            "original_name": "Jackie Chan",
            "popularity": 103.952,
            "profile_path": "/nraZoTzwJQPHspAVsKfgl3RXKKa.jpg",
            "known_for": [
              {
                "adult": false,
                "backdrop_path": "/r4yFYBEcV247B9VXi1307fIhVqN.jpg",
                "id": 2109,
                "title": "Rush Hour",
                "original_language": "en",
                "original_title": "Rush Hour",
                "overview": "When Hong Kong Inspector Lee is summoned to Los Angeles to investigate a kidnapping, the FBI doesn't want any outside help and assigns cocky LAPD Detective James Carter to distract Lee from the case. Not content to watch the action from the sidelines, Lee and Carter form an unlikely partnership and investigate the case themselves.",
                "poster_path": "/we7wOLVFgxhzLzUt0qNe50xdIQZ.jpg",
                "media_type": "movie",
                "genre_ids": [
                  28,
                  35,
                  80
                ],
                "popularity": 49.626,
                "release_date": "1998-09-18",
                "video": false,
                "vote_average": 7.021,
                "vote_count": 4454
              },
              {
                "adult": false,
                "backdrop_path": "/zzFTSEAZcLGSbGipQVflSNUqpij.jpg",
                "id": 5175,
                "title": "Rush Hour 2",
                "original_language": "en",
                "original_title": "Rush Hour 2",
                "overview": "It's vacation time for Carter as he finds himself alongside Lee in Hong Kong wishing for more excitement. While Carter wants to party and meet the ladies, Lee is out to track down a Triad gang lord who may be responsible for killing two men at the American Embassy. Things get complicated as the pair stumble onto a counterfeiting plot. The boys are soon up to their necks in fist fights and life-threatening situations. A trip back to the U.S. may provide the answers about the bombing, the counterfeiting, and the true allegiance of sexy customs agent Isabella.",
                "poster_path": "/aBQf2vMiCINeVC9v6BGVYKXurTh.jpg",
                "media_type": "movie",
                "genre_ids": [
                  28,
                  35,
                  80
                ],
                "popularity": 49.602,
                "release_date": "2001-08-03",
                "video": false,
                "vote_average": 6.719,
                "vote_count": 3738
              },
              {
                "adult": false,
                "backdrop_path": "/ozsLB1HRCN6ZAmJN89pWtoiAwnb.jpg",
                "id": 5174,
                "title": "Rush Hour 3",
                "original_language": "en",
                "original_title": "Rush Hour 3",
                "overview": "After a botched assassination attempt, the mismatched duo finds themselves in Paris, struggling to retrieve a precious list of names, as the murderous crime syndicate's henchmen try their best to stop them. Once more, Lee and Carter must fight their way through dangerous gangsters; however, this time, the past has come back to haunt Lee. Will the boys get the job done once and for all?",
                "poster_path": "/iXsbAIZezI0gh0dnOnBbeFuG3AO.jpg",
                "media_type": "movie",
                "genre_ids": [
                  28,
                  35,
                  80
                ],
                "popularity": 44.166,
                "release_date": "2007-08-08",
                "video": false,
                "vote_average": 6.437,
                "vote_count": 2956
              }
            ]
          },
          {
            "adult": false,
            "gender": 1,
            "id": 1373737,
            "known_for_department": "Acting",
            "name": "Florence Pugh",
            "original_name": "Florence Pugh",
            "popularity": 101.356,
            "profile_path": "/4VghWSpWzH3z2PuJVSKUUZ9thFE.jpg",
            "known_for": [
              {
                "adult": false,
                "backdrop_path": "/aAM3cQmYGjjLQ24m0F0RWfjKQ57.jpg",
                "id": 530385,
                "title": "Midsommar",
                "original_language": "en",
                "original_title": "Midsommar",
                "overview": "Several friends travel to Sweden to study as anthropologists a summer festival that is held every ninety years in the remote hometown of one of them. What begins as a dream vacation in a place where the sun never sets, gradually turns into a dark nightmare as the mysterious inhabitants invite them to participate in their disturbing festive activities.",
                "poster_path": "/7LEI8ulZzO5gy9Ww2NVCrKmHeDZ.jpg",
                "media_type": "movie",
                "genre_ids": [
                  27,
                  18,
                  9648
                ],
                "popularity": 49.56,
                "release_date": "2019-07-03",
                "video": false,
                "vote_average": 7.161,
                "vote_count": 6358
              },
              {
                "adult": false,
                "backdrop_path": "/keIxh0wPr2Ymj0Btjh4gW7JJ89e.jpg",
                "id": 497698,
                "title": "Black Widow",
                "original_language": "en",
                "original_title": "Black Widow",
                "overview": "Natasha Romanoff, also known as Black Widow, confronts the darker parts of her ledger when a dangerous conspiracy with ties to her past arises. Pursued by a force that will stop at nothing to bring her down, Natasha must deal with her history as a spy and the broken relationships left in her wake long before she became an Avenger.",
                "poster_path": "/kwB7d51AIcyzPOBOHLCEZJkmPhQ.jpg",
                "media_type": "movie",
                "genre_ids": [
                  28,
                  12,
                  878
                ],
                "popularity": 72.554,
                "release_date": "2021-07-07",
                "video": false,
                "vote_average": 7.3,
                "vote_count": 9447
              },
              {
                "adult": false,
                "backdrop_path": "/lqxxwTRk0l7CubY6JkJdhnwJEkn.jpg",
                "id": 331482,
                "title": "Little Women",
                "original_language": "en",
                "original_title": "Little Women",
                "overview": "Four sisters come of age in America in the aftermath of the Civil War.",
                "poster_path": "/yn5ihODtZ7ofn8pDYfxCmxh8AXI.jpg",
                "media_type": "movie",
                "genre_ids": [
                  18,
                  10749
                ],
                "popularity": 56.136,
                "release_date": "2019-12-25",
                "video": false,
                "vote_average": 7.894,
                "vote_count": 5799
              }
            ]
          },
          {
            "adult": false,
            "gender": 2,
            "id": 1799766,
            "known_for_department": "Acting",
            "name": "Olivier Renaud",
            "original_name": "Olivier Renaud",
            "popularity": 98.393,
            "profile_path": "/r1PKI00FM4kovA2AQfnWhC1BU13.jpg",
            "known_for": [
              {
                "adult": false,
                "backdrop_path": "/cIGPPTfxKvoezGB4yxmIqa33ROn.jpg",
                "id": 734280,
                "title": "Midnight at the Magnolia",
                "original_language": "en",
                "original_title": "Midnight at the Magnolia",
                "overview": "Longtime friends and local radio hosts Maggie and Jack fake it as a couple for their families and listeners in hopes of getting their show syndicated.",
                "poster_path": "/gOfLcgmRYQ04ihwkNKzM6Vn4CnV.jpg",
                "media_type": "movie",
                "genre_ids": [
                  10749,
                  35,
                  10770
                ],
                "popularity": 9.088,
                "release_date": "2020-11-05",
                "video": false,
                "vote_average": 6.298,
                "vote_count": 213
              },
              {
                "adult": false,
                "backdrop_path": "/6cTPEKkGBDoSfKeljYdNjdIDOrc.jpg",
                "id": 749546,
                "title": "Christmas at Maple Creek",
                "original_language": "en",
                "original_title": "Christmas at Maple Creek",
                "overview": "Sparks fly between a romance novelist and a blacksmith as they try to save a charming village during the holidays.",
                "poster_path": "/pmZ67ddANAEz0aTkDQRCT9FJlIo.jpg",
                "media_type": "movie",
                "genre_ids": [
                  10749,
                  10770,
                  35
                ],
                "popularity": 5.979,
                "release_date": "2020-12-16",
                "video": false,
                "vote_average": 5.5,
                "vote_count": 14
              },
              {
                "adult": false,
                "backdrop_path": "/8azo79XONFzslTjdqZPhPNiAdfy.jpg",
                "id": 882968,
                "title": "Christmas on 5th Avenue",
                "original_language": "en",
                "original_title": "Christmas on 5th Avenue",
                "overview": "Eve, A Christmas loving professional wish granter, takes on the task of decorating the high-end condo for a reclusive writer who is spending the holidays out of town. But when the handsome writer turns out to be home for the holidays after all, Eve will have to pull out all her tricks to make him fall in love with Christmas all over again – and maybe find romance along the way.",
                "poster_path": "/pJE4lG23q6xX1MrxUQtqMXyI46f.jpg",
                "media_type": "movie",
                "genre_ids": [
                  10770,
                  35,
                  10749
                ],
                "popularity": 4.026,
                "release_date": "2021-11-21",
                "video": false,
                "vote_average": 6.8,
                "vote_count": 9
              }
            ]
          }
        
    ];
    
    export default actors;