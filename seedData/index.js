import userModel from '../api/users/userModel';
import users from '../initialise-dev/users';
import dotenv from 'dotenv';
import movieModel from '../api/movies/movieModel';
import movies from '../initialise-dev/movies';

dotenv.config();

// Deletes all user documents in the collection and inserts test data
async function loadUsers() {
  console.log('Loading user data');
  try {
    await userModel.deleteMany();
    await userModel.create(users);
    console.info(`${users.length} users were successfully stored.`);
  } catch (err) {
    console.error(`Failed to load user data: ${err}`);
  }
}

// Deletes all movie documents in the collection and inserts test data
async function loadMovies() {
  console.log('Loading seed data');
  console.log(`Number of movies: ${movies.length}`);
  try {
    await movieModel.deleteMany();
    await movieModel.insertMany(movies);
    console.info(`${movies.length} movies were successfully stored.`);
  } catch (err) {
    console.error(`Failed to load movie data: ${err}`);
  }
}

// Load data only in the development environment
if (process.env.NODE_ENV === 'development') {
  (async () => {
    await loadUsers();
    await loadMovies();
  })();
}
